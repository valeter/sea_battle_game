var fieldHeightCells = 10;
var fieldWidthCells = 10;

var cells = [];

var shipsTotal = 12;

var turnsTotal = 14;

var gamesWon = 0;
var gamesLost = 0;

function newGame() {
	cells = []
	shipsTotal = 12;
	turnsTotal = 14;
	drawField();
	setCurrentScore(shipsTotal, turnsTotal);
}

function highlightShips() {
	var shipsToHighlight = getRandomInt(Math.min(7, shipsTotal), shipsTotal);
	while (true) {
		for (var i = 0; i < fieldHeightCells; i++) {
			for (var j = 0; j < fieldWidthCells; j++) {
				if (cells[i][j] && getRandomBool() && getRandomBool() && getRandomBool()) {
					document.getElementById('cell-' + i + '-' + j);
					highlightShip(i, j);
					shipsToHighlight--;

					if (shipsToHighlight == 0) {
						makeTurn();
						return;
					}
				}
			}
		}
	}
}

function highlightShip(i, j) {
	var cell = document.getElementById('cell-' + i + '-' + j);
	cell.style.backgroundColor = '';
	cell.style.backgroundColor = '#F55';
	setTimeout(function() { 
		unHighlightShip(i, j, ''); 
	}, 400);
}

function unHighlightShip(i, j, color) {
	var cell = document.getElementById('cell-' + i + '-' + j);
	cell.style.backgroundColor = color;
}

function cellOnClick() {
	var str = this.id.substring('cell-'.length);
	var i = parseInt(str[0]);
	var j = parseInt(str[2]);

	if (cells[i][j]) {
		this.classList.add('cell-ship');
		cells[i][j] = false;
		shipsTotal--;
	} else {
		this.classList.add('cell-empty');
	}
	makeTurn();
}

function makeTurn() {
	turnsTotal--;
	setCurrentScore(shipsTotal, turnsTotal);

	if (turnsTotal == 0) {
		finish(shipsTotal == 0);
	} else if (shipsTotal == 0) {
		finish(true);
	} else if (turnsTotal < shipsTotal) {
		finish(false);
	}
}

function finish(win) {
	var message = 'You ';
	if (win) {
		message = message + 'won!';
	} else {
		message = message + 'lost!';
	}
	
	alert(message);
	addGameScore(win);
	newGame();
}

function drawField() {
	var field = document.getElementById('field');
	field.innerHTML = '';

	var currentShipCount = 0;
	while (currentShipCount < shipsTotal) {
		currentShipCount = 0;
		cells = [];
		for (var i = 0; i < fieldHeightCells; i++) {
			cells.push([]);
			for (var j = 0; j < fieldWidthCells; j++) {
				field.appendChild(createCell(i, j));
				var isShip = getRandomBool();
				if (isShip) {
					currentShipCount++;
				}
				cells[i].push(isShip);
			};
		};
	}
}

function createCell(i, j) {
	var cell = document.createElement('div');
	cell.id = 'cell-' + i + '-' + j;
	cell.classList.add('cell');
	cell.onclick = cellOnClick;
	return cell;
}

function setCurrentScore(ships, turns) {
	var scoreDiv = document.getElementById('score-current');
	scoreDiv.innerHTML = 'Ships left:\t' + ships + '</br></br>Turns left:\t' + turns;
}

function addGameScore(win) {
	if (win) {
		gamesWon++;
	} else {
		gamesLost++;
	}

	var scoreDiv = document.getElementById('score-total');
	scoreDiv.innerHTML = 'Games won:\t' + gamesWon + '</br></br>Games lost:\t' + gamesLost;
}

function getRandomBool() {
	var result = getRandomInt(0, 1);
	return result == 1;
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}